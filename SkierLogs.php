<?php

// Classes:

// Info about a particular club
class Club {
    public $id;
    public $name;
	public $city;
    public $county;
}

// Info about a particular skier
class Skier {
    public $userName;
    public $firstName;
	public $lastName;
    public $birthYear;
}

// Whats saved in the season-table, it's the season for a user
class SkierSeason {
    public $skierUser;
    public $clubId;				// Which the particular user had that season
	public $year;
    public $totalDistance;
}


//Get from XML:

function getSkierLogs() {
	$xmlDoc = new DOMDocument();
	$xmlDoc->load("SkierLogs.xml");
	$xpathDoc = new DOMXpath($xmlDoc);
	getClubs($xmlDoc->getElementsByTagName('Club'));
	getSkiers($xpathDoc->query('/SkierLogs/Skiers/Skier'));
	getSeasons($xmlDoc->getElementsByTagName('Season'));
	echo "Finished adding to database.";
}

function getClubs($clubs) {
	for ($i = 0; $i < $clubs->length; $i++) {													// Go through all club-elements.
		$club = new Club;																		// Make a new club.
		$club->id = $clubs->item($i)->getAttribute('id');										// Get (value of) id.
		$club->name = $clubs->item($i)->getElementsByTagName('Name')->item(0)->nodeValue;		// Get name.
		$club->city = $clubs->item($i)->getElementsByTagName('City')->item(0)->nodeValue;		// Get city.
		$club->county = $clubs->item($i)->getElementsByTagName('County')->item(0)->nodeValue;	// Get county.
		addClub($club);																		// Get in database.
		//print_r($club);
	}
}


function getSkiers($skiers) {
	for ($i = 0; $i < $skiers->length; $i++) {															// Go through all skier-elements.
		$skier = new Skier;																				// Make a new club.
		$skier->userName = $skiers->item($i)->getAttribute('userName');									// Get (value of) username.
		$skier->firstName = $skiers->item($i)->getElementsByTagName('FirstName')->item(0)->nodeValue;	// Get first name.
		$skier->lastName = $skiers->item($i)->getElementsByTagName('LastName')->item(0)->nodeValue;		// Get last name.
		$skier->birthYear = $skiers->item($i)->getElementsByTagName('YearOfBirth')->item(0)->nodeValue;	// Get birth year.
		addSkier($skier);																				// Get in database.
		//print_r($skier);
	}
}


function getSeasons($season) {
	for ($i = 0; $i < $season->length; $i++) {											// Go through all club-elements.																		// Make a new club.
		$year = $season->item($i)->getAttribute('fallYear');							// Get year.
		$skiers = $season->item($i)->getElementsByTagName('Skiers');					// Get all skier-elements in season number $i.
		for ($j = 0; $j < $skiers->length; $j++) {										// Go through all skier-elements in season number $i.	
			if ($skiers->item($j)->hasAttribute('clubId')) {							// If in a club.
				$clubId = $skiers->item($j)->getAttribute('clubId');					// Get Club-id.
			}
			else {
				$clubId = null;															// Else it is null.
			}
			$skier = $skiers->item($j)->getElementsByTagName('Skier');					// Get all skier-elements in season number $i.
			for ($k = 0; $k < $skier->length; $k++) {									// Go through all skier-elements in season number $i.
				$skierSeason = new SkierSeason;											// Make a new SkierSeason-object.
				$skierSeason->skierUser = $skier->item($k)->getAttribute('userName');	// Get username of particular user.
				$skierSeason->clubId = $clubId;											// Set club-id in object.
				$skierSeason->year = $year;												// Set year in object.
				$distances = $skier->item($k)->getElementsByTagName('Distance');		// Get all distances (in 'Log/Entry/'-elements).
				$skierSeason->totalDistance = 0;
				for ($l = 0; $l < $distances->length; $l++) {							// Go through all distances in skier number $j.
					$skierSeason->totalDistance += $distances->item($l)->nodeValue;		// + on the value on top of the other that skier has registered.
				}
				addSeason($skierSeason);												// Get in database.
				//print_r($skierSeason);
			}
		}
	}
}


// Add to database

function addSkier($skier) {
	// Add skier
	$db = new PDO('mysql:host=localhost;dbname=assignment5_hestem_473208;charset=utf8', 'root', '', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION) ); 
    $stmt = $db->prepare( 'INSERT INTO skiers (userName, firstName, lastName, yearOfBirth)' . ' VALUES(:user, :fname, :lname, :byear)' );
    $stmt->bindValue(':user', $skier->userName);
    $stmt->bindValue(':fname', $skier->firstName);
    $stmt->bindValue(':lname', $skier->lastName);
	$stmt->bindValue(':byear', $skier->birthYear);
    $affectedNo = $stmt->execute();
    return true;
}


function addClub($club) {
	// Add club
	$db = new PDO('mysql:host=localhost;dbname=assignment5_hestem_473208;charset=utf8', 'root', '', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION) ); 
    $stmt = $db->prepare( 'INSERT INTO clubs (id, name, city, county)' . ' VALUES(:id, :name, :city, :county)' );
    $stmt->bindValue(':id', $club->id);
    $stmt->bindValue(':name', $club->name);
    $stmt->bindValue(':city', $club->city);
	$stmt->bindValue(':county', $club->county);
    $affectedNo = $stmt->execute();
    return true;
}

function addSeason($skierSeason) {
	// Add season/skier-info
	$db = new PDO('mysql:host=localhost;dbname=assignment5_hestem_473208;charset=utf8', 'root', '', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION) ); 
    $stmt = $db->prepare( 'INSERT INTO seasons (skierUser, clubId, year, totalDistance)' . ' VALUES(:sUser, :cId, :year, :totalDistance)' );
    $stmt->bindValue(':sUser', $skierSeason->skierUser);
    $stmt->bindValue(':cId', $skierSeason->clubId);
    $stmt->bindValue(':year', $skierSeason->year);
	$stmt->bindValue(':totalDistance', $skierSeason->totalDistance);
    $affectedNo = $stmt->execute();
    return true;
}


getSkierLogs();
?>