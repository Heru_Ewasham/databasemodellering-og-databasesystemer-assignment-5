-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 05. Nov, 2017 13:53 PM
-- Server-versjon: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `assignment5_hestem_473208`
--

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `clubs`
--

CREATE TABLE `clubs` (
  `id` varchar(100) COLLATE utf8_danish_ci NOT NULL,
  `name` varchar(500) COLLATE utf8_danish_ci NOT NULL,
  `city` varchar(500) COLLATE utf8_danish_ci NOT NULL,
  `county` varchar(500) COLLATE utf8_danish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `seasons`
--

CREATE TABLE `seasons` (
  `skierUser` varchar(100) COLLATE utf8_danish_ci NOT NULL,
  `clubId` varchar(100) COLLATE utf8_danish_ci DEFAULT NULL,
  `year` int(10) NOT NULL,
  `totalDistance` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `skiers`
--

CREATE TABLE `skiers` (
  `userName` varchar(100) COLLATE utf8_danish_ci NOT NULL,
  `firstName` varchar(250) COLLATE utf8_danish_ci NOT NULL,
  `lastName` varchar(250) COLLATE utf8_danish_ci NOT NULL,
  `yearOfBirth` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_danish_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clubs`
--
ALTER TABLE `clubs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seasons`
--
ALTER TABLE `seasons`
  ADD PRIMARY KEY (`skierUser`,`year`) USING BTREE,
  ADD KEY `clubId` (`clubId`),
  ADD KEY `skierUser` (`skierUser`);

--
-- Indexes for table `skiers`
--
ALTER TABLE `skiers`
  ADD PRIMARY KEY (`userName`);

--
-- Begrensninger for dumpede tabeller
--

--
-- Begrensninger for tabell `seasons`
--
ALTER TABLE `seasons`
  ADD CONSTRAINT `seasons_ibfk_1` FOREIGN KEY (`SkierUser`) REFERENCES `skiers` (`userName`) ON UPDATE CASCADE,
  ADD CONSTRAINT `seasons_ibfk_2` FOREIGN KEY (`clubId`) REFERENCES `clubs` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `seasons_ibfk_3` FOREIGN KEY (`skierUser`) REFERENCES `skiers` (`userName`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
