This is my answer to Assignment 5 in "Databasemodelering databasesystemer" 2017.

The answer to task 1 is in the pdf delivered.

The image files is the answer to task 2.1 (it's just two filetypes, elsewhere they are the same).

The sql file is answer to task 2.2.

The php file is answer to task 2.3 (and the xml file is the file we shall export to database).